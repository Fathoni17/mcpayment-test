MCPayment Software code

## To run you can use
```
npm install
npm run start
```
or 
```
yarn
yarn start
```

## To test you can use
```
npm run test
```
or 
```
yarn test
``` 