import assert from 'assert';
import solutions from '../src/solutions.js';

describe('Problem 3 testing', () => {
  it(`should return [ ${['loud', 'four', 'lost']} ]`, () => {
    assert.deepStrictEqual(solutions.filterWord('souvenir loud four lost', 4), ['loud', 'four', 'lost'])
  });
  it(`should return [ ${['souvenir']} ]`, () => {
    assert.deepStrictEqual(solutions.filterWord('souvenir loud four lost', 8), ['souvenir'])
  });
  it(`should return [ ]`, () => {
    assert.deepStrictEqual(solutions.filterWord('hello world good morning', 3), [])
  });
  it(`should return [ ${[ 'hello', 'world' ]} ]`, () => {
    assert.deepStrictEqual(solutions.filterWord('hello world good morning', 5), ['hello', 'world'])
  });
}); 